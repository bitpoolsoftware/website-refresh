import { applyMiddleware, createStore } from 'redux';
import rootReducer from './rootReducer';
import createSagaMiddleware from 'redux-saga';



 
// Logger with default options
import logger from 'redux-logger';

//sagas
import { watchSetAuthUser } from '../sagas/auth';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
  rootReducer,
  applyMiddleware(logger, sagaMiddleware)
)

var WatchFunctions =[
watchSetAuthUser
]
WatchFunctions.forEach(watchFunction => {
  sagaMiddleware.run(watchFunction);
});
export default store;

