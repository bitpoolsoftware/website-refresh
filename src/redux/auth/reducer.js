import { 
    UNSET_AUTH_USER,
    SET_AUTH_USER_ASYNC,
    CHECK_VALID_TOKEN_ASYNC
  } from './types';
  
  
  import axios from 'axios'
  
   
  const envConfig = require('../../config.json');
  
  const INITIAL_STATE = {
    user:null,
    IsLoggedIn: false
  }
  
  const reducer = (state = INITIAL_STATE, action) =>  {
    switch (action.type) {
      // On set authenticated user
      case SET_AUTH_USER_ASYNC:
          return {
            ...state, 
            user:{
              email:  action.payload.email,
              token:  action.payload.token,
            }
          };
        
      default:
        return state;
    }
  }
  
  export default reducer;