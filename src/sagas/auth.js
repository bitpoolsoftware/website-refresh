import {takeEvery, put} from 'redux-saga/effects';
import envConfig from '../config.json'
import Cookies from 'universal-cookie';
const axios = require('axios');


//auth user async function
function* authUserAsync(action){
    var authedUser;
    yield axios.post(envConfig.host + '/api/authenticate', {
        email: action.payload.user.email,
        password: action.payload.user.password
      })
      .then(function (response) {
          authedUser = {
              email: response.data.email,
              token: response.data.token
          }
          const cookies = new Cookies();
          cookies.set('token', response.data.token);
      })
      .catch(function (error){
        authedUser = {
           auth: false,
           reason: error.response.status
        }
      }); 
      yield put({type: 'SET_AUTH_USER_ASYNC', payload: authedUser})
}




export function* watchSetAuthUser(){
    yield takeEvery('SET_AUTH_USER', authUserAsync);
}
