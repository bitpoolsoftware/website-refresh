import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import MainNav from '../src/components/common/MainNav';
import SocialNav from '../src/components/common/SocialNav';
import styled from 'styled-components';


import Home from '../src/views/Home/Home.js'
import About from '../src/views/About/About.js';
import ForCustomer from '../src/views/ForCustomer/ForCustomer.js';
import ForIntegrators from '../src/views/ForIntegrators/ForIntegrators.js';
import Deepend from '../src/views/Deepend/Deepend';
import Contact from '../src/views/Contact/Contact';

import EdgeProducts from '../src/views/EdgeProducts/EdgeProducts';
import SoftwareProducts from '../src/views/SoftwareProducts/SoftwareProducts';
import CloudProducts from '../src/views/CloudProducts/CloudProducts';

const NavWrapper = styled.div` 
    position: fixed;
    top: 0;
    left: 0;
    z-index: 999;
    width: 100%;
    height: 23px;
`;

function App() {
  return (
    <Router>
    <NavWrapper>
    <SocialNav/>
    <MainNav/>
    </NavWrapper>
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/About" component={About}/>
      <Route exact path="/ForCustomer" component={ForCustomer}/>
      <Route exact path="/ForIntegrators" component={ForIntegrators}/>
      <Route exact path="/Deepend" component={Deepend}/>
      <Route exact path="/Contact" component={Contact}/>
      <Route exact path="/Edge-Products" component={EdgeProducts} />
      <Route exact path="/Software-Products" component={SoftwareProducts}/>
      <Route exact path="/Cloud-Products" component={CloudProducts}/>
    </Switch>
    </Router>
  );
}

export default App;
