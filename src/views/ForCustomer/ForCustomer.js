import React, {Component} from 'react';
import HeroSection from '../ForCustomer/components/HeroSection';
import ProblemSolvedSection from '../ForCustomer/components/ProblemSolvedSection';
import HexgonSection from '../ForCustomer/components/HexagonSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
import HexagonSection from '../ForCustomer/components/HexagonSection';


class ForCustomer extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <HexagonSection/>
      <ProblemSolvedSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default ForCustomer;