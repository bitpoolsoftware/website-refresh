import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForCustomer/HexagonSection.css';
import HexagonSrc from '../../../assets/images/solveHexagons.png';


const SolveHexagon = styled.img`
height: 30em;
margin-top: 3em;
@media (max-width: 720px) {
  height: 20em;
  }
`;


const HexagonSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="HexagonSection">
        <Container className="centering" fluid>
          <Row className="justify-content-center" >
            <SolveHexagon src={HexagonSrc}/>  
            <div className="mt-5">
            <h1 style={{color:"rgb(60, 191, 243)"}}>
                Solve
            </h1>
            <p className="mb-5">Our pool of knowledge and capability provides <br/>custom solutions to discover and manage <br/>property data for people, places and communities. </p>

            <h1 style={{color:"rgb(60, 191, 243)", marginLeft:"4em"}}>
                Create
            </h1>
            <p style={{ marginLeft:"10em"}}>We create technologies which unlock possibilities,<br/> helping building managers identify issues in real-time. </p>

            <h1  className="mt-5" style={{color:"rgb(60, 191, 243)"}}>
                Innovate
            </h1>
            <p>Our pool of knowledge and capability provides <br/>custom solutions to discover and manage <br/>property data for people, places and communities. </p>
            </div>
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default HexagonSection;