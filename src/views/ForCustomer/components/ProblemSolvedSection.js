import React from 'react';
import { Jumbotron, Container, Row, Col, Button } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/About/ProblemSolvedSection.css';


const ProblemImg = styled.img`
     @media (max-width: 720px) {
        width: 100%;
  }
`;

const ProblemSolvedSection = (props) => {
  return(
    <div>
      <div  className="ProblemSolvedSection text-center">
        <Container fluid>
            <h1 style={{color: "#3CBFF3"}}>Problem Solved</h1>
            <p>Read through the different range of projects completed in the past to give you an idea of solutions delivered.</p>
            <Row>
                <Col style={{marginBottom: "7em"}}>
                <ProblemImg  src="http://placekitten.com/g/564/332"/>
                <p className="text-muted mt-3">28/04/2021</p>
                <h4 style={{color: "#3CBFF3"}}>Integrated Building Platform For South Everly
 Technology Park </h4>
                </Col>
                <Col>
                <ProblemImg  src="http://placekitten.com/g/564/332"/>
                <p className="text-muted mt-3">28/04/2021</p>
                <h4 style={{color: "#3CBFF3"}}>Integrated Building Platform For South Everly
 Technology Park </h4>
                </Col>
            </Row>

            <Row>
                <Col>
                <ProblemImg src="http://placekitten.com/g/439/271"/>
                <p className="text-muted mt-3">28/04/2021</p>
                <h4 style={{color: "#3CBFF3"}}>Integrated Building Platform For South Everly
 Technology Park </h4>
                </Col>
                <Col>
                <ProblemImg src="http://placekitten.com/g/439/271"/>
                <p className="text-muted mt-3">28/04/2021</p>
                <h4 style={{color: "#3CBFF3"}}>Integrated Building Platform For South Everly
 Technology Park </h4>
                </Col>
                <Col>
                <ProblemImg src="http://placekitten.com/g/439/271"/>
                <p className="text-muted mt-3">28/04/2021</p>
                <h4 style={{color: "#3CBFF3"}}>Integrated Building Platform For South Everly
 Technology Park </h4>
                </Col>
            </Row>
            <Row className="">

            <Col className="text-center">
            <Button className="cta-button" outline="true">View More</Button>
            </Col>

            </Row>
        </Container>
      </div>  
    </div>
  );
};

export default ProblemSolvedSection;