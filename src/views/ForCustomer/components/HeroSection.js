import React from 'react';
import { Jumbotron, Container} from 'reactstrap';

import '../../../css/components/CommonHeroSection.css';

const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
     
      <Container fluid className="TextWrapper">
        <h3 className="display-4">Bitpool Platform</h3>
        <p className="lead under-text display-5">Bitpool is an innovative data intelligence platform which<br/> connects and universally translates building data into<br/> meaningful and real-time property and human insights. 
</p>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection;