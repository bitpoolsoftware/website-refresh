import React, {Component} from 'react';


import BitpoolVsSrc from '../../assets/images/bitpoolvs.png';
import CCTVSrc from '../../assets/images/cctv.png';
import ChargingSrc from '../../assets/images/charging.png';
import SmartSpaceSrc from '../../assets/images/smartspace.png';
import DeependSrc from '../../assets/images/deepend.png';
import LightDriverSrc from '../../assets/images/lightdriver.png';
import lockbitSrc from '../../assets/images/lockbit.png';
import PeopleSrc from '../../assets/images/people.png';
import OpenBookSrc from '../../assets/images/openbook.png';


import HeroSection from '../EdgeProducts/components/HeroSection';
import ImageLeftSection from '../EdgeProducts/components/ImageLeftSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
import ImageRightSection from '../EdgeProducts/components/ImageRightSection';
import CtpSection from '../SoftwareProducts/components/CtpSection';
import ChallengeSection from '../SoftwareProducts/components/ChallengeSection';
class EdgeProducts extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <CtpSection/>
      <ChallengeSection/>
      <ImageRightSection title={"Bitpool People Places"} text={"Introducing Bitpool People Places, an innovative platform responsible for collecting true data from People and Places, and making it readily available to a user in the form that meets their needs."} ImgSrc={PeopleSrc}/>
      <ImageLeftSection title={"Bitpool Smart Space"} text={"Introducing Smart Space People Sensor, an adaptable system designed to better understand people density within your space.Built using open infrastructure to provide affordable solutions to overcome today's challenge in the building space and align with Government social distancing requirements."} ImgSrc={SmartSpaceSrc}/> 
      <ImageRightSection title={"Bitpool Deepend"} text={"Introducing Bitpool Deepend, a powerful online forum which brings the building technology community together to discuss and help solve real problems in the Prop – Tech space."} ImgSrc={DeependSrc}/>
      <ImageLeftSection title={"Bitpool Openbook"} text={"Introducing Bitpool OpenBook, a powerful product built with NodeRed, for Commercial Tenants to reserve office spaces efficiently for any occasion. Spaces will be ready and waiting for the users with the optimal temperature/ conditions for comfort."} ImgSrc={OpenBookSrc}/> 
      <ImageRightSection title={"Bitpool VS"} text={"Visualise data from core system building technology to monitor and maintain building/sin real time. Using a range of widgets data can be viewed as simple bar charts to regression analysis to visualise more complex data sets."} ImgSrc={BitpoolVsSrc}/>
      <ImageLeftSection title={"Emergency Lighting Driver"} text={"Introducing the new Bitpool Legrand Lighting Driver, a smart solution that integrates with all the Emergency Lighting signs within a building. A customised driver built specifically for the Tridium infrastructure."} ImgSrc={LightDriverSrc}/> 
      <ImageRightSection title={"EO  Vehicle Charging Driver"} text={"Introducing the new Bitpool EO Electric Vehicle Charging Driver, a smart solution that integrates with EO electric vehicles in real-time. A customised Driver built specifically, for the Tridium Infrastructure."} ImgSrc={ChargingSrc}/>
      <ImageLeftSection title={"Lockit Driver"} text={"The Lockit Driver integrates with smart Lockers to enable building owners to monitor and control the use of lockers in your building"} ImgSrc={lockbitSrc}/> 
      <ImageRightSection title={"Cctv Driver"} text={"Introducing the new Bitpool CCTV Streaming Driver, a smart solution that integrates with CCTV."} ImgSrc={CCTVSrc}/>
      
  
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default EdgeProducts;