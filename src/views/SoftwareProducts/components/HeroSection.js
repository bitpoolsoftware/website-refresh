import React from 'react';
import { Jumbotron, Container} from 'reactstrap';
import '../../../css/components/CommonHeroSection.css';

const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
      <Container fluid className="TextWrapper">
      <h3 className="display-4">Building Intelligence Technologies</h3>
        <p className="lead under-text display-5">Our products create smart workplaces by integrating building technologies to create <br/> a central source of information that empowers accurate planning and boosts <br/> building performance. </p>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection