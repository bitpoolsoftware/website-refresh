import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';


import '../../../css/views/SoftwareProducts/ChallengeSection.css';

const ChallengeSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ChallengeSection align-middle text-center">
        <Container className="" fluid>
            <h1 style={{color: "#3CBFF3"}} className="mb-5">Challenges</h1>
            <p className="mb-0">
            Historically, the Prop Tech industry has been an area that does not collaborate well with industry partners, technical engineers, end users and property owners.<br/>
            Today, building owners, operators and tenants that occupy these spaces are looking to technology to provide better experiences for the occupants. There is an insatiable appetite by building owners and operators for innovation and technology advancements predominantly to remain relevant and competitive in the property space to potential tenants.<br/>
            The Industry however has failed to keep up with the times and defaults to its past solutions.
            </p>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ChallengeSection;