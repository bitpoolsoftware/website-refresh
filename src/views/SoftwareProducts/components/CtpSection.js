import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForCustomer/HexagonSection.css';
import HexagonSrc from '../../../assets/images/solveHexagons.png';


const SolveHexagon = styled.img`
height: 30em;
margin-top: 3em;
@media (max-width: 720px) {
  height: 20em;
  }
`;


const CtpSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="HexagonSection">
        <Container className="centering" fluid>
          <Row className="justify-content-center" >
            <SolveHexagon src={HexagonSrc}/>  
            <div className="mt-5">
            <h1 style={{color:"rgb(60, 191, 243)"}}>
                Connect
            </h1>
            <p className="mb-5">We connect anything to anyone,<br/> anywhere.</p>

            <h1 style={{color:"rgb(60, 191, 243)", marginLeft:"4em"}}>
                Translate
            </h1>
            <p style={{ marginLeft:"10em"}}>We translate true data from different systems <br/> into a central pool of  <br/>meaningful property and human insights.</p>

            <h1  className="mt-5" style={{color:"rgb(60, 191, 243)"}}>
                Predict
            </h1>
            <p>We empower users to predict behaviours <br/> and functionality to <br/> optimise property performance.</p>
            </div>
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default CtpSection;