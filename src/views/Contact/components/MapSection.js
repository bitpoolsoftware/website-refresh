import React from 'react'
import { GoogleMap, useJsApiLoader, Marker } from '@react-google-maps/api';
import BitpoolMarker from '../../../assets/images/BitpoolMarker.png';


const center = {
  lat: -27.561850,
  lng: 152.945140
};

function Map() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyCsAEVm4KQLen9s-F9fPjaFzpGk2rdtMig"
  })

  const [map, setMap] = React.useState(null)

  const onLoad = React.useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds();
    map.fitBounds(bounds);
    setMap(map)
  }, [])

  const onUnmount = React.useCallback(function callback(map) {
    setMap(null)
  }, [])

  return isLoaded ? (
      <GoogleMap
        mapContainerStyle={{
            height: "50vh",
            width: "100%"
          }}
        center={center}
        zoom={50}
        onLoad={onLoad}
        onUnmount={onUnmount}
      >
        { /* Child components, such as markers, info windows, etc. */ }
        <>
        <Marker
      icon={BitpoolMarker}
      position={center}
    />
        </>
      </GoogleMap>
  ) : <></>
}

export default React.memo(Map)