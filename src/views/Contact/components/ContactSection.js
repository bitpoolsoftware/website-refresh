import React from 'react';
import { Jumbotron, Container, Button,Row, Col, InputGroup, Input } from 'reactstrap';

import '../../../css/views/Contact/ContactSection.css';
import PhoneSrc from '../../../assets/images/phone.png';
import EmailSrc from '../../../assets/images/email.png';
import MarkerSrc from '../../../assets/images/marker.png';

const ContactSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ContactSection text-center">
        <Container fluid>
            <Row>
                <Col sm>
                <div className="text-left">
                <h1 style={{color:"rgb(0, 173, 239)"}}>We'd love to hear from you</h1>
                 <Row className="mt-3"><img src={MarkerSrc} className="mr-4 ml-4" /> <span>29 Sudbury Street Darra, Brisbane, QLD, Australia</span></Row> 
                 <Row className="mt-3"><img src={PhoneSrc} className="mr-4 ml-4" /> <span>Phone: 1300 4 BITPOOL</span></Row>  
                 <Row className="mt-3"><img src={EmailSrc} className="mr-4 ml-4" /> <span>Email: support @bitpool.com</span></Row> 
                </div>
                
                </Col>
                <Col sm>
                <div style={{width: "70%"}}>
                    <Input className="mb-4"  style={{backgroundColor:"rgba(0, 0, 0, 0)", borderColor: "#748A96", color:"white"}} placeholder="Name" type="text"/>
                    <Input className="mb-4" style={{backgroundColor:"rgba(0, 0, 0, 0)",borderColor: "#748A96", color:"white"}} placeholder="Email" type="email"/>
                    <Input className="mb-4" style={{backgroundColor:"rgba(0, 0, 0, 0)",borderColor: "#748A96", color:"white"}} placeholder="Reason For Enquiry" type="text"/>
                    <Input className="mb-4" style={{backgroundColor:"rgba(0, 0, 0, 0)",borderColor: "#748A96", color:"white"}} placeholder="Write Something Here..." type="textarea"/>
                    <Button style={{width:"100%", backgroundColor: "#00ADEF"}}>Submit</Button>

                </div>
                </Col>
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ContactSection;