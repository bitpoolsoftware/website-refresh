import React from 'react';
import { Jumbotron, Container} from 'reactstrap';
import DeependLogoSrc from '../../../assets/images/DeependLogo.png';
import '../../../css/components/CommonHeroSection.css';
import styled from 'styled-components'

const DeependLogo = styled.img`

`;
const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
      <Container fluid className="TextWrapper">
      <h3 className="display-4">Contact Us</h3>
        <p className="lead under-text display-5">Please reach out to our friendly staff to help find <br/>the right solution for you!</p>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection