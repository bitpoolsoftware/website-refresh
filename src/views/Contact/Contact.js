import React, {Component} from 'react';

import HeroSection from '../Contact/components/HeroSection';
import ContactSection from '../Contact/components/ContactSection';
import Map from '../Contact/components/MapSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
class Deepend extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <ContactSection/>
      <Map/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default Deepend;