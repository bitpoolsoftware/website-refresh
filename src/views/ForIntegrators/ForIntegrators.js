import React, {Component} from 'react';

import HeroSection from '../ForIntegrators/components/HeroSection';
import TridiumOEMSection from '../ForIntegrators/components/TridiumOEM';
import TimeLineSection from '../ForIntegrators/components/TimeLine';
import HowWeHelp from '../ForIntegrators/components/HowWeSupport';
import Benefits from '../ForIntegrators/components/Benefits';
import BecomeSection from '../ForIntegrators/components/BecomeSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
class About extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <TridiumOEMSection/>
      <TimeLineSection/>
      <HowWeHelp/>
      <Benefits/>
      <BecomeSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default About;