import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForIntegrators/TridiumOEM.css';

import MacBookSrc from '../../../assets/images/become.png';

const MacBook = styled.img`
height: 50vh;
margin-top: 4em;
margin-left:15em;
@media (max-width: 720px) {
  margin-top:0em;
margin-left:0em;
height: 36vh;
  }
`;

const RightText = styled.h1`
color: #00ADEF;
margin-bottom: 1em;
margin-top: 2em;


`;



const BecomeSection = (props) => {
  return(
    <div>
      <div className="TridiumSection text-center">
        <Container fluid>
        <Row>
            <Col className="">
            <MacBook src={MacBookSrc}/>
            </Col>
            <Col className="">
            <RightText>Become An Integrator</RightText>
            <p>
            We maintain high standards and only work with partners <br/>authorised and trained to implement and manage<br/> the Bitpool platform. 
            If you would like to become <br/> a Certified Bitpool Integrator, download<br/> the integrator pack to learn more. 
            </p>
            <Row>
            <div className="flex jusitfy-content-center buttonDiv">
            <Button className="cta-button m-3" outline="true">Download Now</Button>
            <Button className="cta-button m-3" outline="true">Register your interest</Button>
            </div>
            </Row>
            </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default BecomeSection;