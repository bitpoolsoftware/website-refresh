import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';
import BenefitImageSrc from '../../../assets/images/benefits.png';
import '../../../css/views/ForIntegrators/Benefits.css';


const BenefitImage = styled.img`
height: 50vh;

`;


const Benefits = (props) => {
  return(
    <div>
      <div className="Benefits text-center">
        <Container fluid>
        <Row>
            <Col className="">
            <h1 style={{color:"#00ADEF"}}>Integrator Benefits</h1>
            <BenefitImage src={BenefitImageSrc}/>
           </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default Benefits;