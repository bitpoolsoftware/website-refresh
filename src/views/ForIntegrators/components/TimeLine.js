import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForIntegrators/TimeLine.css';

import TimeLineSrc from '../../../assets/images/Timeline.png';

const TimeLine = styled.img`
margin-top: 0;
padding: 10em;
padding-top: 6em;
@media (max-width: 720px) {
  padding: 0em;
padding-top: 0em;
width:100%;
  }
`;



const TimelineSection = (props) => {
  return(
    <div>
      <div className="Timeline text-center">
        <Container fluid>
        <Row>
            <Col>
            <h1 style={{color:"#00ADEF"}}>How It Works</h1>
           <TimeLine src={TimeLineSrc}/>
           </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default TimelineSection;