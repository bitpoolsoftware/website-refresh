import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForIntegrators/HowWeHelp.css';




const HowWeHelp = (props) => {
  return(
    <div>
      <div className="HowWeHelpSection text-center">
        <Container fluid>
        <Row>
            <Col className="TextSection">
            <h1 style={{color:"#00ADEF"}}>How We Support You </h1>
            <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit,<br/> sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br/>Ut enim ad minim veniam, quis nostrud exercitation ullamco<br/> laboris nisi ut aliquip ex ea commodo consequat. <br/>Duis aute irure dolor in reprehenderit in voluptate 
            </p>
           </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default HowWeHelp;