import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/ForIntegrators/TridiumOEM.css';

import MacBookSrc from '../../../assets/images/TridiumOEM.png';

const MacBook = styled.img`
height: 50vh;
margin-top: 0;
margin-left:15em;
@media (max-width: 720px) {
  margin-left: 0em;
  }
`;

const RightText = styled.h1`
color: #00ADEF;
margin-bottom: 1em;
margin-top: 2em;
`;



const TridiumOEMSection = (props) => {
  return(
    <div>
      <div className="TridiumSection text-center">
        <Container fluid>
        <Row>
            <Col className="">
            <MacBook src={MacBookSrc}/>
            </Col>
            <Col className="">
            <RightText className="dispay-1">Tridium OEM Reseller</RightText>
            <p>
            Tridium is an OEM based product and a global leader in business<br/> application frameworks that have fundamentally changed the way<br/> devices and systems connect around the world.<br/> 
            Bitpool is a Tridium OEM <br/>who offers both Bitpool branded Tridium products and VYKON branded Tridium products.
            </p>

            </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default TridiumOEMSection;