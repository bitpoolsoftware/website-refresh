import React from 'react';
import { Jumbotron, Container} from 'reactstrap';

import '../../../css/components/CommonHeroSection.css';

const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
      <Container fluid className="TextWrapper">
        <h3 className="display-4">Our Platform</h3>
        <p className="lead under-text display-5">A software that empowers its users to make real-time decisions using true data<br/> about the ongoing performance and health of a user’s building in a simple way.</p>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection;