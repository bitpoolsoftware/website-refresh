import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Home/SolveSection.css';
import HexagonSrc from '../../../assets/images/solveHexagons.png';
const SolveHexagon = styled.img`
height: 23em;
margin-top: 3em;
margin-right:9em;
`;
const mobile = require('is-mobile');

const SolveSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="SolveSection text-center">
        <Container fluid>
        <Row className="SolveSectionInner"> 
          <Col className="mt-5 SolveText">
           <h1>Solve</h1> <br/>
           <p className="text-white">
           We Connect anything, to<br/> 
          Anyone, Any where
           </p>
           <Button className="cta-button" outline="true">Learn more</Button>
          </Col>
          {mobile() ? null : 
          <Col className="SolveCol">
              <SolveHexagon src={HexagonSrc}/>
          </Col> }
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default SolveSection;