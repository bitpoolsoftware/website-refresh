import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Home/InnovateSection.css';
import HexagonSrc from '../../../assets/images/innovateHex.png';


const CreateHexagon = styled.img`
height: 23em;
margin-top: 3em;
margin-right:9em;
`;
const mobile = require('is-mobile');


const InnovateSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="InnovateSection text-center">
        <Container fluid>
        <Row className=""> 
          <Col className="mt-5 InnovateText">
           <h1>Innovate</h1> <br/>
           <p className="text-white">
           We Bring the future forward by <br/>predicting user behaviour and asset <br/> performance
           </p>
           <Button className="cta-button" outline="true">Learn more</Button>
          </Col>
          {mobile() ? null :
          <Col className="InnovateCol">
              <CreateHexagon src={HexagonSrc}/>
          </Col>}
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default InnovateSection;