import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';
import '../../../css/views/Home/DeependSection.css';

import ConnectSrc from '../../../assets/images/Connect.png';
import TranslateSrc from '../../../assets/images/Translate.png';
import PredictSrc from '../../../assets/images/Predict.png';



const Badge = styled.span`
height: 7em;
`;

const DeependSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="DeependSection">
        <Container fluid>
        <Row className=""> 
          <Col className="text-center">
          <h1 style={{color:"#00ADEF"}}>At Bitpool We:</h1>
          </Col>
        </Row>
        <Row className="text-center mt-5">
            <Col>
            <Badge>
                <img src={ConnectSrc} style={{height: "7em"}} />
                <h4>Connect</h4>
                <p><span style={{color:"#00ADEF"}}>Connect</span> anything to anyone,anywhere.</p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={TranslateSrc} style={{height: "7em"}} />
                <h4>Translate</h4>
                <p><span style={{color:"#00ADEF"}}>Translate</span> data from different systems into a central pool of  meaningful property and human insights.</p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={PredictSrc} style={{height: "7em"}}/>
                <h4>Predict</h4>
                <p>Empower users to <span style={{color:"#00ADEF"}}>predict</span> behaviour and functionality to optimise property performance.</p>
            </Badge>
            </Col>
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default DeependSection;