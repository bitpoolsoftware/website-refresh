import React from 'react';
import { Jumbotron, Container, Button } from 'reactstrap';

import '../../../css/views/Home/HeroSection.css';
import video from '../../../assets/video/videobg.mp4'

const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="Hero">
      <div>
        <video autoPlay muted loop>
          <source src={video} id="vid" data-src={video} type="video/mp4" />
        </video>
      </div>
      <Container fluid className="TextWrapper">
        <h3 className="HeadingText"> Building Intelligence <p> Technology </p></h3>
        <p className="lead under-text display-5">Bitpool Technologies turn inanimate passive<br/>building environments into intelligently<br /> automated people places</p>
        <Button className="cta-button" outline="true">Learn more</Button>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection;