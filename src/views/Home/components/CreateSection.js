import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Home/CreateSection.css';
import HexagonSrc from '../../../assets/images/createHexagons.png';
const mobile = require('is-mobile');

const CreateHexagon = styled.img`
height: 23em;
margin-top: 3em;
margin-right:9em;
`;


const CreateSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="CreateSection text-center">
        <Container fluid>
        <Row className=""> 
          <Col className="mt-5 CreateText">
           <h1>Create</h1> <br/>
           <p className="text-white">
           We Humanise places by translating <br/> data, making building smarter
           </p>
           <Button className="cta-button" outline="true">Learn more</Button>
          </Col>
          {mobile() ? null :
          <Col className="SolveCol">
              <CreateHexagon src={HexagonSrc}/>
          </Col>}
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default CreateSection;