import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Home/PlatformSection.css';
import MacBookSrc from '../../../assets/images/MacBook.png';

const MacBook = styled.img`
height: 50vh;
margin-top: 10em;
margin-left:15em;
@media (max-width: 1366px) {
    margin-right:0em;
    margin-bottom: 0em;
    margin-top: 0em;
    margin-left:0em;
    height:34vh;
  }
`;

const RightText = styled.h1`
color: #00ADEF;
margin-right:10em;
margin-bottom: 1em;
margin-top: 2em;
`;



const PlatformSection = (props) => {
  return(
    <div>
      <div  className="PlatformSection text-center">
        <Container fluid>
        <Row>
            <Col className="LeftSection">
            <MacBook src={MacBookSrc}/>
            </Col>
            <Col className="RightSection">
            <RightText className="dispay-1">Bitpool Platform</RightText>
            <Card body className="PlatformCardStep text-left" style={{backgroundColor: "#242833"}}>
                <h1>
                    Connect
                </h1>
                <p>Dynamically connect and translate building data into meaningful, real time property insights. </p>
            </Card>
            <Card body className="PlatformCardStep text-left" style={{backgroundColor: "#242833"}}>
                <h1>
                    Translate
                </h1>
                <p>Intuitively translate highly complex data insights to enable proactive building experience responses. </p>
            </Card>
            <Card body className="PlatformCardStep text-left" style={{backgroundColor: "#242833"}}>
                <h1>
                    Predict
                </h1>
                <p>Efficiently predict asset value building management strategies that align to positive tenant and visitor experiences. </p>
            </Card>
            </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default PlatformSection;