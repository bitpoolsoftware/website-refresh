import React, {Component} from 'react';
import HeroSection from '../Home/components/HeroSection';
import ConnectSection from '../Home/components/ConnectSection';
import SolveSection from '../Home/components/SolveSection';
import CreateSection from '../Home/components/CreateSection';
import InnovateSection from '../Home/components/InnovateSection';
import DeependSection from '../Home/components/DeependSection';
import PlatformSection from '../Home/components/PlatformSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
class Home extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <ConnectSection/>
      <SolveSection/>
      <CreateSection/>
      <InnovateSection/>
      <DeependSection/>
      <PlatformSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default Home;