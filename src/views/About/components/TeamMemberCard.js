import { faPeopleArrows } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import {Card} from 'reactstrap';

const TeamMemberCard = (props) => {
  return (
    <div>
      <Card  className="TeamMemberCard">
        <img className="TeamMemberCardImg" src={props.TeamMemberImgSrc} />
        <span style={{position:"absolute", bottom:"0", marginBottom:"0.7em", marginLeft:"2em", color:"#3CBFF3"}}>
        <h3>
          {props.name}
        </h3>
        <span style={{color:"white", float: 'left'}}>{props.title}</span>
        </span>
      </Card>
    </div>
  );
};

export default TeamMemberCard;
