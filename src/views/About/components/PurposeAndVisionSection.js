import React from 'react';
import { Jumbotron, Container, Button,Row, Col, Card } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/About/PurposeAndVision.css';
import RightImageSrc from '../../../assets/images/PurposeAndVision.png'

const RightImage = styled.img`

`;

const PurposeAndVisionSection = (props) => {
  return(
    <div>
      <div  className="PurposeAndVisionSection text-center">
        <Container fluid>
        <Row className="mt-5">
            <Col>
            <h1 className="PurposeAndVisionTitle"> Purpose & Vision</h1>
            <p>
            Bitpool is a building technology creator and innovator with a mission to solve real problems in the prop-tech space which is achieved by listening to all perspectives of the building technology community – from customer to technology provider.<br/>
                <br className="mt-5"/>
We believe in using technology to improve a person's experience inside Buildings with affordable long-term solutions. By investing our time into the people and bringing them along the journey, our fine group of technologists can provide customers with the best experience during all stages of their interaction.
            </p>
            </Col>
            <Col>
             <RightImage src={RightImageSrc}/>
            </Col>
        </Row>
        </Container>
      </div>  
    </div>
  );
};

export default PurposeAndVisionSection;