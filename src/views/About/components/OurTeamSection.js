import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';
import TeamMemberCard from '../components/TeamMemberCard';
import '../../../css/views/About/OurTeam.css';


const OurTeamSection = (props) => {
  return(
    <>
      <div  className="text-center">
        <Container fluid>
        <Row className="TopText">
            <Col>
            <h1 style={{color: "rgb(0, 173, 239)"}}>Our Team</h1>
            <p>Get to know our specialised range of technologists who help create a better future for you!</p>
            </Col>
        </Row>
        <Row className="TeamImagesSection">
        <Col>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1920&height=1080" name="David Blanch" title="CTO"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?hex=4fcb94&width=800&height=600" name="Ethan Jones" title="Applications Engineer"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1820&height=1080" name="Armin Kalabic" title="Lead Developer" />
        </Col>
        <Col>
        <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1920&height=1080" name="Sean Wratten" title="Technology Manager"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?hex=4fcb94&width=800&height=600" name="Ash Abdullrabzak" title="UX and Design Graphics Manager"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1820&height=1080" name="Natasha Sisanto" title="Graphics UI Designer"/>
        </Col>
        <Col>
        <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1920&height=1080" name="Phil Smith" title="Senior Engineer"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?hex=4fcb94&width=800&height=600"  name="Ihsaan Mahomed" title="UX UI  Graphics Designer"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1820&height=1080" name="Sujith Sadanandan" title="Graphics UI Designer"/>
        </Col>
        <Col>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1920&height=1080" name="Shilpi Chattopadhyay" title="Business Development Manager "/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?hex=4fcb94&width=800&height=600" name="Malak Othman" title="Graphics Designer"/>
           <TeamMemberCard TeamMemberImgSrc="https://www.colorbook.io/imagecreator.php?width=1820&height=1080" name="Chhorn Moung" title="IT Solution Engineer"/>
        </Col>
        </Row>

        </Container>
      </div>  
    </>
  );
};

export default OurTeamSection;