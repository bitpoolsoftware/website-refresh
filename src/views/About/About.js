import React, {Component} from 'react';

import CommonHeroSection from '../../components/common/CommonHeroSection';
import PurposeAndVisionSection from '../About/components/PurposeAndVisionSection';
import OurTeamSection from '../About/components/OurTeamSection';
import ProblemSolvedSection from '../About/components/ProblemSolvedSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
class About extends Component {
  render() {
    return(
      <>
      <CommonHeroSection/>
      <PurposeAndVisionSection/>
       <OurTeamSection/>
      <ProblemSolvedSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default About;