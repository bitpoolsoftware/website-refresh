import React, {Component} from 'react';

import HeroSection from '../Deepend/components/HeroSection';
import ExpertsSection from '../Deepend/components/ExpertsSection';
import JoinDeependSection from '../Deepend/components/JoinDeepend';

import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
const Mobile = require('is-mobile');

class Deepend extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      {Mobile() ? null :
       <ExpertsSection/>} 
       <JoinDeependSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default Deepend;