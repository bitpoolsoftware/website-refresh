import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Deepend/Experts.css';

import ConnectSrc from '../../../assets/images/Connect.png';
import InnovateSrc from '../../../assets/images/Innovate.png';
import TechnologySrc from '../../../assets/images/Technology.png';



const Badge = styled.div`

`;

const ExpertsSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ExpertsSection">
        <Container fluid>
        <Row className="text-center mt-5">
            <Col>
            <Badge>
                <img src={ConnectSrc} style={{height: "7em"}} />
                <h4 style={{color:"#00ADEF"}}>Connect with experts</h4>
                <p>Connect with property tech experts to learn how to leverage technology to lower operational costs and improve tenant and user experiences. </p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={TechnologySrc} style={{height: "7em"}} />
                <h4 style={{color:"#00ADEF"}}>Solve Technology Challenges </h4>
                <p>Predict the future by learning about the latest property tech innovations and connect with other community members to discover solutions to challenges.</p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={InnovateSrc} style={{height: "7em"}}/>
                <h4 style={{color:"#00ADEF"}}>Discover Innovative Solutions </h4>
                <p>Learn from the community about this challenges they face and how you can help to deliver innovative solutions. </p>
            </Badge>
            </Col>
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ExpertsSection;