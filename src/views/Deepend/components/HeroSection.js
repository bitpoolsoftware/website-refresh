import React from 'react';
import { Jumbotron, Container} from 'reactstrap';
import DeependLogoSrc from '../../../assets/images/DeependLogo.png';
import '../../../css/components/CommonHeroSection.css';
import styled from 'styled-components'

const DeependLogo = styled.img`
 @media (max-width: 1366px) {
    margin-right:0em;
    height:6em;
  }
`;
const HeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
      <Container fluid className="TextWrapper">
        <DeependLogo src={DeependLogoSrc} />
        <p className="lead under-text display-5">The Bitpool DeepEnd is an open community where people who are passionate<br/> about solving real problems in the property tech space can connect and drive <br/> innovation beyond the functional and discover how technology can be used <br/>to benefit building owners, tenants and users. </p>
      </Container>
    </Jumbotron>
  );
};

export default HeroSection