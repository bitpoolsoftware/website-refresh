import React from 'react';
import { Jumbotron, Container, Button,Row, Col, InputGroup, Input } from 'reactstrap';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import '../../../css/views/Deepend/JoinDeepend.css';

const JoinDeependSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="JoinDeependSection text-center">
        <Container fluid>
            <Row>
                <Col>
                <div className="text-left">
                <h1 style={{color:"rgb(0, 173, 239)"}}>Join The Global Community</h1>
                <h4>Are you ready for the future?<br/> 
                Click on the link below to join the conversations.</h4>
                <a style={{fontSize: "2em", color: "#00ADEF"}}  target="_blank" href="//deepend.bitpool.com">www.deepend.bitpool.com</a>
                </div>
                
                </Col>
                <Col>
                <div style={{width: "70%"}}>
                    <Input className="mb-4"  style={{backgroundColor:"rgba(0, 0, 0, 0)", borderColor: "#748A96", color:"white"}} placeholder="Name" type="text"/>
                    <Input className="mb-4" style={{backgroundColor:"rgba(0, 0, 0, 0)",borderColor: "#748A96", color:"white"}} placeholder="Email" type="email"/>
                    <Input className="mb-4" style={{backgroundColor:"rgba(0, 0, 0, 0)",borderColor: "#748A96", color:"white"}} placeholder="Write Something Here..." type="textarea"/>
                    <Button style={{width:"100%", backgroundColor: "#00ADEF"}}>Submit</Button>

                </div>
                </Col>
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default JoinDeependSection;