import React, {Component} from 'react';


import EdgeSrc from '../../assets/images/Edge.png';
import IOTliteSrc from '../../assets/images/IOTlite.png';
import OpenDataSrc from '../../assets/images/OpenData.png';
import OSSrc from '../../assets/images/OS.png';
import OutOfBoxSrc from '../../assets/images/OutOfBox.png';
import EdgeIOTSrc from '../../assets/images/Picture_3.png';
import SmartSrc from '../../assets/images/Smart.png';


import HeroSection from '../EdgeProducts/components/HeroSection';
import ImageLeftSection from '../EdgeProducts/components/ImageLeftSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';
import ImageRightSection from './components/ImageRightSection';
import CustomerSection from './components/CustomerSection';
import DeependSection from './components/DeependSection';
class EdgeProducts extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <ImageLeftSection title={"OPEN out of the box"} text={"Bitpool products are built on OPEN technologies that removes any barrier to entry for developing or access to data.Bitpool is an OPEN data eco-system that allows anyone of any skill set to create value from the data that is collected"} ImgSrc={OutOfBoxSrc}/> 
      <ImageRightSection title={"Bitpool OS"} text={"Bitpool OS is a software that empowers its users to make real-time decisions using true data about the ongoing performance and health of a user’s building in a simple way."} ImgSrc={OSSrc}/>
      <ImageLeftSection title={"Open Data Eco System"} text={"Powered by the Dell Edge Gateway 3002 and Bitpool OS the IoT Lite is more intelligent, capable and scalable than a standard sensor interface and consolidates the need for separate systems. "} ImgSrc={OpenDataSrc}/> 
      <ImageRightSection title={"Bitpool IOT Lite"} text={"Powered by the Dell Edge Gateway 3002 and Bitpool OS the IoT Lite is more intelligent, capable and scalable than a standard sensor interface and consolidates the need for separate systems."} ImgSrc={IOTliteSrc}/>
      <ImageLeftSection title={"Bitpool IOT Edge"} text={"Powered by the Dell Edge Gateway 3002 and Bitpool OS, it is more intelligent, capable and scalable than a standard sensor interface and consolidates the need for separate systems."} ImgSrc={EdgeSrc}/> 
      <ImageRightSection title={"Bitpool Edge"} text={"Powered by the Niagara Framework , Bitpool Edge seamlessly integrates with both legacy and modern OPEN protocols such as BACnet to enable simplified connectivity at the Edge ."} ImgSrc={EdgeIOTSrc}/>
      <ImageLeftSection title={"Smart Sense BLE/IEQ"} text={"Introducing Bitpool Smart Sense IEQ/BLE, another powerful product built to better understand the quality of indoor environment in community spaces.Uses Bluetooth to scan Mac address’s and verify the data within your building is correct."} ImgSrc={SmartSrc}/> 
      <CustomerSection/>
      <DeependSection/> 
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default EdgeProducts;