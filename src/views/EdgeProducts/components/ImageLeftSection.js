import React from 'react';
import { Jumbotron, Container,Row, Col, Button } from 'reactstrap';

import '../../../css/views/EdgeTechnology/EdgeTechnology.css';

const Mobile = require("is-mobile");

const ImageLeftSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ImageLeftSection ">
        <Container fluid>
            <Row>
              {Mobile() ? null :
                <Col>
                <img  style={{height:"90%", width:"100%"}}className="LeftSectionImage" src={props.ImgSrc} />
                </Col>}
                <Col className="align-middle">
                <div className="mt-5">
                <h1 className="TitleOfLeft">{props.title}</h1>
                <p className="display-5">{props.text}</p>
                <Button className="cta-button mr-5" outline="true">Learn more</Button>
                </div>
                </Col>
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ImageLeftSection;