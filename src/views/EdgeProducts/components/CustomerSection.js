import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Deepend/Experts.css';

import CustomerSrc from '../../../assets/images/customer.png';
import IntegratorSrc from '../../../assets/images/integrator.png';
import DeveloperSrc from '../../../assets/images/developer.png';



const Badge = styled.div`

`;

const CustomerSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className=" CustomerSection">
        <Container fluid>
        <Row className="text-center mt-5">
            <Col>
            <Badge>
                <img src={CustomerSrc} style={{height: "7em"}} />
                <h4 style={{color:"#00ADEF"}}>Customer</h4>
                <p>Connect with property tech experts to learn how to leverage technology to lower operational costs and improve tenant and user experiences.</p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={IntegratorSrc} style={{height: "7em"}} />
                <h4 style={{color:"#00ADEF"}}>Integrator</h4>
                <p>Connect with property tech experts to learn how to leverage technology to lower operational costs and improve tenant and user experiences.</p>
            </Badge>
            </Col>
            <Col>
            <Badge>
                <img src={DeveloperSrc} style={{height: "7em"}}/>
                <h4 style={{color:"#00ADEF"}}>Developer</h4>
                <p>Learn from the community about the challenges they face and how you can help to deliver innovative solutions. 
</p>
            </Badge>
            </Col>
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default CustomerSection;