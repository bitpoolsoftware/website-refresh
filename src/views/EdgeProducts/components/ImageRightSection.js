import React from 'react';
import { Jumbotron, Container,Row, Col, Button } from 'reactstrap';

import '../../../css/views/EdgeTechnology/EdgeTechnology.css';
const Mobile = require("is-mobile");
const ImageRightSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ImageRightSection ">
        <Container fluid>
            <Row>
                <Col className="align-middle">
                <div className="mt-5">
                <h1 className="TitleOfRight">{props.title}</h1>
                <p className="display-5">{props.text}</p>
                <Button className="cta-button mr-5" outline="true">Learn more</Button>
                </div>
                </Col>
                {Mobile() ? null :
                <Col>
                <img style={{height:"90%", width:"80%"}} className="RightSectionImage" src={props.ImgSrc} />
                </Col>}
            </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ImageRightSection;