import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';
import styled from 'styled-components';

import '../../../css/views/Home/DeependSection.css';
import DeependLogoSrc from '../../../assets/images/DeependLogo.png';
import LightBulbSrc from '../../../assets/images/lightbulb.png';
import BuildingSrc from '../../../assets/images/buildings.png';
import PeopleTalkingSrc from '../../../assets/images/peopleTalking.png';



const DeependLogo = styled.img`
 margin-right: 6em;
 @media (max-width: 1366px) {
    margin-right:0em;
    height:6em;
  }
`;

const Badge = styled.span`
height: 7em;

`;

const DeependSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="DeependSection">
        <Container fluid>
        <Row className=""> 
          <Col className="text-center">
              <DeependLogo src={DeependLogoSrc} />
              <p className="DeependText">
              The Bitpool DeepEnd is an open community where people who are passionate about solving real problems in the<br/> property tech space can connect and drive innovation beyond the functional and discover how technology can be used to <br/> benefit building owners, tenants and users. 
              </p>
              <Button className="cta-button" outline="true">Learn more</Button>    
          </Col>
        </Row>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default DeependSection;