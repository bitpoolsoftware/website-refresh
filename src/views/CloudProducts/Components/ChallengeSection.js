import React from 'react';
import { Jumbotron, Container, Button,Row, Col } from 'reactstrap';


import '../../../css/views/SoftwareProducts/ChallengeSection.css';

const ChallengeSection = (props) => {
  return(
    <div>
      <Jumbotron fluid className="ChallengeSectionCloud align-middle text-center">
        <Container className="" fluid>
            <h1 style={{color: "#3CBFF3"}} className="mb-5">Challenges</h1>
            <p className="display-5 mb-0">
            The challenge that is being faced by millions of users within the building industry is the lack of<br/> access to data (real-time or through feedback options). Many building owners are still using<br/> manual systems to collect information which results in <br/> data manipulation and inconsistencies in the captured samples.
            </p>
        </Container>
      </Jumbotron>    
    </div>
  );
};

export default ChallengeSection;