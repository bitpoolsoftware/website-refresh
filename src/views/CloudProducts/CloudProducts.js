import React, {Component} from 'react';


import GcloudSrc from '../../assets/images/gcloud.png';

import HeroSection from '../CloudProducts/Components/HeroSection';
import ChallengeSection from '../CloudProducts/Components/ChallengeSection';
import ImageOnRight from '../EdgeProducts/components/ImageRightSection';
import CustomerSection from '../CloudProducts/Components/CustomerSection';
import DeependSection from '../EdgeProducts/components/DeependSection';
import Footer from '../../components/common/Footer';
import SocialFooter from '../../components/common/SocialFooter';

const Mobile = require('is-mobile');
class CloudProducts extends Component {
  render() {
    return(
      <>
      <HeroSection/>
      <ChallengeSection/>
     
      <ImageOnRight CloudProducts={true} title={"Google Cloud Gcp Driver"} text={"Bitpool is a building technology creator and innovator with a mission to solve real problems in the proptech space which is achieved by listening to all perspectives of the building technology community – from customer to technology provider."} ImgSrc={GcloudSrc}/>
      {Mobile() ? null :
      <CustomerSection/> } 
      <DeependSection/>
      <Footer/>
      <SocialFooter/>
      </>
    );
  }
}

export default CloudProducts;