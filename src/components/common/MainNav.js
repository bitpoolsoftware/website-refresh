import React, {useState, useEffect} from 'react';
import '../../css/components/MainNav.css';
import {
  Collapse,
  Navbar,
  Nav,
  NavbarBrand,
  NavItem,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  NavbarToggler
} from 'reactstrap';
import {NavLink, Link} from 'react-router-dom'
import styled from 'styled-components'
import LogoUrl from '../../assets/images/bitpool-logo.png';
const isMobile = require("is-mobile");
const Logo = styled.img`
height: 3.5em;
margin-left: 8em;
@media (max-width: 720px) {
  margin-left:0em;
  }
`;

const LinkText = styled.span`
font-size: 1.5em;
`;



const MainNav = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = ()  =>  isMobile() ? setIsOpen(!isOpen) : null 
  return (
    <Navbar  dark className="MainNavBar" expand="md">
        <NavbarBrand><Logo src={LogoUrl}/></NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
        <Nav className="NavInner flex-row" navbar>
            <NavItem className="ml-5 mt-1 text-white" onClick={toggle}><NavLink className="NavLink" exact  activeClassName='NavLinkActive' id="home" to="/">Home</NavLink></NavItem>
            <NavItem className="ml-5 mt-1 text-white" onClick={toggle}><NavLink className="NavLink" exact activeClassName='NavLinkActive' id="customer" to="/ForCustomer">For Customer</NavLink></NavItem>
            <NavItem className="ml-5 mt-1 text-white" onClick={toggle}><NavLink className="NavLink" exact activeClassName='NavLinkActive' id="integrators" to="/ForIntegrators">For Integrators</NavLink></NavItem>
            <UncontrolledDropdown className="ml-5 mt-1 text-white">
            <DropdownToggle nav className="NavLink"  id="technology" style={{ padding: "0 !important"}}>
            Technology
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <Link onClick={toggle}  to="/Edge-Products">Edge Products</Link>
              </DropdownItem>
              <DropdownItem>
              <Link onClick={toggle} to="/Software-Products">Software Products</Link>
              </DropdownItem>
              <DropdownItem>
              <Link onClick={toggle} to="/Cloud-Products">Cloud Products</Link>
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
            <NavItem className="ml-5 mt-1 text-white"><NavLink onClick={toggle} className="NavLink" exact  activeClassName='NavLinkActive' id="deepend" to="/Deepend">Deepend</NavLink></NavItem>
            <NavItem className="ml-5 mt-1 text-white"><NavLink  onClick={toggle} className="NavLink" exact  activeClassName='NavLinkActive' id="contact" to="/Contact">Contact</NavLink></NavItem>
        </Nav> 
        </Collapse>
      </Navbar>
);
  
}

export default MainNav;