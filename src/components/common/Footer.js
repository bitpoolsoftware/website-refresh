import React from 'react';
import { Jumbotron, Container, Button,Row, Col,Input,InputGroupAddon, InputGroupText,InputGroup} from 'reactstrap';
import styled from 'styled-components';

import '../../css/components/Footer.css';
import LogoUrl from '../../assets/images/bitpool-logo-slider.png';
const mobile = require('is-mobile');
const Logo = styled.img`
height: 3.5em;
`;
const Footer = (props) => {
  return(
      <Jumbotron fluid className="Footer">
        <Container fluid>
            <Row className="InnerFooter">
                <Col>
                <Logo src={LogoUrl} className="mb-3"/><br/>
                <span className="text-muted">
                29 Sudbury Street, Darra QLD 2076.
                support@bitpool.com.au</span>
                </Col>
                <Col>
                <h5 style={{color:"#00ADEF"}}>Quick Links</h5>
                <a href="www.google.com" className="text-muted">For Customers</a><br/>
                <a href="www.google.com" className="text-muted">For Integrators</a><br/>
                <a href="www.google.com" className="text-muted">Technology</a><br/>
                <a href="www.google.com" className="text-muted">Deepend</a><br/>
                </Col>
                <Col>
            
                <a href="www.google.com" className="text-muted">About</a><br/>
                <a href="www.google.com" className="text-muted">Contact</a><br/>
                <a href="www.google.com" className="text-muted">Case Studies</a><br/>
                <a href="www.google.com" className="text-muted">Downloads</a><br/>
                </Col>
                <Col>
                
                <a href="www.google.com" className="text-muted">Articles</a><br/>
                <a href="www.google.com" className="text-muted">Shop</a><br/>
                <a href="www.google.com" className="text-muted">Login</a>
                </Col>
                <Col className="mr-3">
                <h5 style={{color:"#00ADEF"}}>Subscribe to our Newsletter</h5>
                <p className="text-muted">Enter your email address to subscribe our<br/>newsletter</p>
                <Row>
                <InputGroup>
                <Input style={{height:"3em",backgroundColor:"#1B2732", borderColor:"#1B2732"}} placeholder="Email" />
                 <Button style={{height:"3em",backgroundColor:"#00ADEF", borderColor:"#00ADEF",borderRadius: "0px"}} >Submit</Button>
                </InputGroup>     
                </Row>
                </Col>
            </Row>
        </Container>
      </Jumbotron>
   
  );
  
};

export default Footer;