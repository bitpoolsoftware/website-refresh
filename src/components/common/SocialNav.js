import React from 'react';
import '../../css/components/SocialNav.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebook, faLinkedin, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import {Link} from 'react-router-dom'
import {
  Navbar,
  Nav,
  NavbarText,
  NavbarBrand
} from 'reactstrap';
import styled from 'styled-components'

const Text = styled.span`
color: #3CBFF3;
margin: 0.5em;
`;

const SocialNav = (props) => {
  return (
    <div>
      <Navbar className="SocialNavBar">
          <Nav className="mr-auto" navbar>
          </Nav>
          <NavbarText className="RightText">
               <FontAwesomeIcon className="SocialIcon" icon={faYoutube} /> 
               <FontAwesomeIcon className="SocialIcon" icon={faLinkedin} />  
               <FontAwesomeIcon className="SocialIcon" icon={faFacebook} /> 
               <FontAwesomeIcon className="SocialIcon" icon={faTwitter} />
               <Link to="/About"><Text>About</Text></Link>
               {/* <Text>Shop</Text>
               <Text>|</Text> */}
               {/* <Text><FontAwesomeIcon className="LoginIcon" icon={faUser}/> Login</Text> */}
           </NavbarText>
      </Navbar>
    </div>
  );
}

export default SocialNav;