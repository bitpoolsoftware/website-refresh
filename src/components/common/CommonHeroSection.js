import React from 'react';
import { Jumbotron, Container} from 'reactstrap';

import '../../css/components/CommonHeroSection.css';

const CommonHeroSection = (props) => {
  return (
    <Jumbotron fluid className="CommonHero">
     
      <Container fluid className="TextWrapper">
        <h3 className="display-4">We solve,Create, <p>and innovate </p></h3>
        <p className="lead under-text display-5">We are an innovation company, specialising in the creation<br/> of technology solutions which enable real building data to<br/> be collected and activated. 
</p>
      </Container>
    </Jumbotron>
  );
};

export default CommonHeroSection;