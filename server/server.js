const express = require('express');
const path = require('path');
var app = express();


app.use(express.static(path.join(__dirname, '../build')));


// Start server
app.listen(3000);